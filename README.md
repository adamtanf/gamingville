[Link to Backend](https://bitbucket.org/adamtanf/gamingville-be/src/master/)



<div align="center">
      <h1> <br/>GAMINGVILLE</h1>
     </div>
>

# Description
A fully functional gaming website and and server.

# Features
- The site hosts 2 open source games.
- My own creation-A cool arcade game called Graduation road.
- Support for Multiplayer scenarios using WebSockets.
- User hierarchy with different options for different user roles.
# Screenshots
![a](mdAssets/1.png)
![b](mdAssets/2.png)
![c](mdAssets/3.png)
![d](mdAssets/4.png)
![e](mdAssets/5.png)
![f](mdAssets/6.png)
# Tech Used
 ![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E) ![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white) ![HTML5](https://img.shields.io/badge/html5-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white) ![Angular](https://img.shields.io/badge/angular-%23DD0031.svg?style=for-the-badge&logo=angular&logoColor=white) ![Bootstrap](https://img.shields.io/badge/bootstrap-%23563D7C.svg?style=for-the-badge&logo=bootstrap&logoColor=white) ![Apache Maven](https://img.shields.io/badge/Apache%20Maven-C71A36?style=for-the-badge&logo=Apache%20Maven&logoColor=white) ![MySQL](https://img.shields.io/badge/mysql-%2300f.svg?style=for-the-badge&logo=mysql&logoColor=white)
      
# How To Install:
### You can add How to Setup:
- Step 1: Install open jdk version 11 [here](https://openjdk.java.net/).
- Step 2: Install MySql [here](https://www.mysql.com/downloads/) and run the queries located here: game-app/src/main/resources/db-scripts. then run the following 
    - create-scema.sql
    - entities.sql
- step 3: Open a terminal window at the game-app subfolder located under the back end project and run the run the following commands:
    - mcnw package/.
    - mvnw spring-boot:run/.
    - Now the back end will run through port 8080 at the following address: http://localhost:8080/
- step 4: instarr node js [here](https://nodejs.org/en/download/)
- step 5: Install angular cli [here](https://angular.io/cli)
step 6: Open a terminal at the main folder game-app-client of the front end project and run the following commands: 
    - npm install 
    - ng serve 
    - Now the front end project will run through port 4200 at: http:localhost:4200

