import { FormControl, ValidationErrors } from "@angular/forms";

export class NoWhiteSpaceValidator {
  static notOnlyWhitespace(control: FormControl) : ValidationErrors{
    // check if string only contains whitespace
    if ((control.value != null) && (control.value.trim().length === 0))
    {
      // invalid, return error object
      return { 'notOnlyWhitespace': true};// the html template will check for this error key and accordingly display error message or not
    }
    else{
      //valid return null
      return null;

    }
  }
}
