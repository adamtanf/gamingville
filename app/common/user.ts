export class User {
  userName: string;
  email: string;
  identification: string;
  phoneNumber: string;
  password: string;
  previousUserName: string;
  previousIdentification: string;
}
