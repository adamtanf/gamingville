import {BlackListDto} from "./black-list-dto";
import {Role} from "./role";

export class UserListDto {
  userName: string;
  identification: string;
  roles: Role[];
  active: boolean;
  blackList: BlackListDto;
}
