export class Game {
  id: string;
  name: string;
  description: string;
  duration: number;
  gameLink: string;
}
