import {UserDto} from './user-dto';

export class UserAuthDto {
  token: string;
  user: UserDto;
}
