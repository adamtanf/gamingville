import {User} from './user';

export class BlackList {
  comment: string;
  dateCreated: Date;
  user: User;
}
