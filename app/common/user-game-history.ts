export class UserGameHistory {
  date: Date;
  gameName: string;
  score: number;
}
