export class RegisterSinglePlayerGame {
  name: string;
  identification: string;
  score: number;
  gameId: string;
}
