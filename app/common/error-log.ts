import {User} from './user';

export class ErrorLog {
  dateCreated: Date;
  content: string;
  user: User;
}
