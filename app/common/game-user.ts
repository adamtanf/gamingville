import {User} from './user';
import {Game} from './game';

export class GameUser {
  user: User;
  game: Game;
  score: number;
}
