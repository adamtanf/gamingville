import {BlackListDto} from './black-list-dto';

export class UserDto {
  name: string;
  identification: string;
  roles: string[];
  active: boolean;
  blackList: BlackListDto;
}
