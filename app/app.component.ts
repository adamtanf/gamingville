import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  isLogin;
  constructor(private service : UserService, private router: Router){

  }

  signOut(){
    localStorage.clear();
this.router.navigate(["login"]);
this.service.updateMyUser(false);
  }
  ngOnInit(): void {
    //this.isAdminUser = this.isUserHasAdmin();
    this.isUserlogin();
    this.isUserlogin2();
  }
  isUserlogin(){
    this.isLogin = this.service.getUserInfo();
  }
  isUserlogin2(){
    this.service.$user.subscribe(x=>{
      this.isLogin = x;
    });
  }
  title = 'game-app-client';
}
