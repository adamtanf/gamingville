import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserService} from "./user.service";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url = 'http://localhost:8080';

  constructor(private http: HttpClient, private userService: UserService) { }

  generateToken(credentials: any) {
    // token generate
    const json = {
      userName: credentials.username,
      password: credentials.password
    };
    console.log(json);
    return this.http.post(`${this.url}/authenticate`, json);

  }

  //for login user
  /*loginUser(token) {
    localStorage.setItem('token', token);
    return true;
  }*/

  loginUser(token, user) {
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
    return true;
  }

  isLoggedIn() {
    const token = localStorage.getItem('token');
    if (token == undefined || token === '' || token == null) {
      return false;
    }

    return true;
  }

  logout() {
    localStorage.removeItem('token');
    return true;
  }

  getToken() {
    return localStorage.getItem('token');
  }

  getUser() {
    return JSON.parse(localStorage.getItem('user'));
  }

  getUserInfo() {
    return this.http.get(`${this.url}/getusers`);
  }
}
