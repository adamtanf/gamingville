import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BlackList} from '../common/black-list';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BlackListService {

  private baseUrl = `${environment.gameAppApiUrl}/blackLists`;

  constructor(private httpClient: HttpClient) { }

  getBlackListList(thePage: number,
                  thePageSize: number): Observable<GetBlackListResponse> {

    const url = `${this.baseUrl}?projection=blackListProjection&page=${thePage}&size=${thePageSize}`;
    console.log(url);
    return this.httpClient.get<GetBlackListResponse>(url);
  }
}

interface GetBlackListResponse{
  _embedded: {
    blackLists: BlackList[];
  };
  page: {
    size: number,
    totalElements: number,
    totalPages: number,
    number: number,
  };
}
