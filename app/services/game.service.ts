import { Injectable } from '@angular/core';
import {UserService} from "./user.service";
import {RegisterSinglePlayerGame} from "../common/register-single-player-game";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Game} from "../common/game";

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private baseUrl = environment.gameAppUrl;
  constructor(private userService: UserService, private httpClient: HttpClient) { }

  getUserGameDetails(id, score) {
    const user = this.userService.getUser();
    const dto = new RegisterSinglePlayerGame();
    dto.gameId = id;
    dto.score = score;
    dto.name = user.name;
    dto.identification = user.identification;

    return dto;
  }

  registerGame(id, score) {
    const dto = this.getUserGameDetails(id, score);
    const url = `${this.baseUrl}/game/register-game`;
    this.httpClient.post(url, dto).subscribe(
      response => {
        console.log('registerGame response: ' + response);
      },
      error => {
        console.log('registerGame response: ' + error);
      });
  }

  getAllGames() {
    const searchUrl = `${environment.gameAppApiUrl}/games`;
    console.log('inside getAllGames');
    console.log(searchUrl);
    return this.httpClient.get<GetGamesResponse>(searchUrl);
  }

  getUserGamesHistory() {
    const gameHistoryUrl = `${this.baseUrl}/game/games-history`;

    return this.httpClient.post(gameHistoryUrl, this.userService.getUserAuth());
  }
}

interface GetGamesResponse{
  _embedded: {
    games: Game[];
  };
}
