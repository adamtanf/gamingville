import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {environment} from "../../environments/environment";
import {BlackList} from "../common/black-list";
import {UserDto} from "../common/user-dto";
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userPush: BehaviorSubject<any> = new BehaviorSubject(false);
  public $user = this.userPush.asObservable();

  updateMyUser(data : any){
    this.userPush.next(data);
  }

  private baseUrl = environment.gameAppUrl;
  constructor(private httpClient: HttpClient, private router: Router) { }

  registerUser(user) {
    const url = `${this.baseUrl}/register`;

    return this.httpClient.post(url, user);
  }

  updateUser(user) {
    const url = `${this.baseUrl}/user/update-user`;
    console.log('user details before server');
    console.log(user);
    return this.httpClient.post(url, user);
  }

  loginUser(token, user) {
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
    return true;
  }

  isLoggedIn() {
    const token = localStorage.getItem('token');
    if (token == undefined || token === '' || token == null) {
      return false;
    }

    return true;
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    return true;
  }

  getToken() {
    return localStorage.getItem('token');
  }

  getUser() {
    return JSON.parse(localStorage.getItem('user'));
  }

  getUserInfo() {
    return this.httpClient.get(`${this.baseUrl}/getusers`);
  }

  getUserDetails(userDto) {
    const url = `${this.baseUrl}/user/getUserDetails`;
    return this.httpClient.post(url, userDto);
  }

  isUserAdmin() {
    try{
      const user = this.getUser();
      const roles = user.roles;
      const usersUrl = `${environment.gameAppUrl}/user/admin`;

      const adminRole = roles.find(element => element === 'ROLE_ADMIN');
      console.log(`adminRoles: ${adminRole}`);
      if (adminRole === 'ROLE_ADMIN'){
        /*this.httpClient.post(usersUrl, this.getUserAuth()).subscribe(
          (response: boolean) => {
            console.log(response);
            if (response == true) {
              return true;
            }
          },
          error => {
            console.log(error);
            return false;
          });*/
        return true;

      }
    }catch (error) {
      return false;
    }

    return false;
  }

  getUserAuth() {
    return {
      name: this.getUser().name,
      identification: this.getUser().identification
    };
  }

  getListOfUsers(thePage, thePageSize) {
    const usersUrl = `${environment.gameAppApiUrl}/users/?projection=userProjection&page=${thePage}&size=${thePageSize}`;

    return this.httpClient.get<GetUserResponse>(usersUrl);
  }

  unBlockUser(json) {
    const blockUserUrl = `${environment.gameAppUrl}/user/unblock-user`;
    return this.httpClient.post(blockUserUrl, json);
  }

  blockUser(json) {
    const blockUserUrl = `${environment.gameAppUrl}/user/block-user`;
    return this.httpClient.post(blockUserUrl, json);
  }

  isUserNameValid(userName) {
    const url = `${environment.gameAppUrl}/welcome/check-user-name`;

    return this.httpClient.post(url, userName);
  }
}

interface GetUserResponse{
  _embedded: {
    users: UserDto[];
  };
  page: {
    size: number,
    totalElements: number,
    totalPages: number,
    number: number,
  };
}
