import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ErrorLog} from '../common/error-log';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErrorLogService {

  private baseUrl = `${environment.gameAppApiUrl}/errorLogs`;

  constructor(private httpClient: HttpClient) { }

  getErrorLogList(thePage: number,
                  thePageSize: number): Observable<GetErrorLogResponse> {

    const url = `${this.baseUrl}?projection=errorLogProjection&page=${thePage}&size=${thePageSize}`;
    console.log(url);
    return this.httpClient.get<GetErrorLogResponse>(url);
  }
}

interface GetErrorLogResponse{
  _embedded: {
    errorLogs: ErrorLog[];
  };
  page: {
    size: number,
    totalElements: number,
    totalPages: number,
    number: number,
  };
}
