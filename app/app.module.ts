import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {Routes, RouterModule, Router} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import {AuthInterceptorService} from './services/auth-interceptor.service';
import { ErrorLogComponent } from './components/error-log/error-log.component';
import { BlackListComponent } from './components/black-list/black-list.component';
import { MainAppViewComponent } from './components/main-app-view/main-app-view.component';
import { ManagementComponent } from './components/management/management.component';
import { UserListComponent } from './components/user-list/user-list.component';
import {LoginService} from './services/login.service';
import {AuthGuard} from './services/auth.guard';
import { MultiGameComponent } from './components/multi-game/multi-game.component';
import { GamesViewComponent } from './components/games-view/games-view.component';
import { LoginRegistrationComponent } from './components/login-registration/login-registration.component';
import { GamesHistoryComponent } from './components/games-history/games-history.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { CardGameComponent } from './components/games/card-game/card-game.component';
import { GraduationroadComponent } from './components/games/graduation-road/graduation-road.component';
import { GeneralErrorComponent } from './components/general-error/general-error.component';
import {UserService} from "./services/user.service";
import { AboutComponent } from './components/about/about.component';

const routes: Routes = [
  {path: 'user-details/:identification/:name', component: UserDetailsComponent, canActivate: [AuthGuard]},
  {path: 'error', component: GeneralErrorComponent},
  {path: 'user-list', component: UserListComponent, canActivate: [AuthGuard]},
  {path: 'user-details', component: UserDetailsComponent, canActivate: [AuthGuard]},
  {path: 'games-history', component: GamesHistoryComponent, canActivate: [AuthGuard]},
  {path: 'user-registration', component: LoginRegistrationComponent},
  {path: 'games', component: GamesViewComponent, canActivate: [AuthGuard]},
  {path: 'tic-tac-toe', component: MultiGameComponent, canActivate: [AuthGuard]},
  {path: 'main', component: MainAppViewComponent, canActivate: [AuthGuard]},
  {path: 'management', component: ManagementComponent, canActivate: [AuthGuard]},
  {path: 'blackList', component: BlackListComponent, canActivate: [AuthGuard]},
  {path: 'errorLog', component: ErrorLogComponent, canActivate: [AuthGuard]},
  {path: 'users', component: UserListComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'card-game', component: CardGameComponent, canActivate: [AuthGuard]},
  {path: 'graduation-road', component: GraduationroadComponent, canActivate: [AuthGuard]},
  {path: 'about', component: AboutComponent},
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: '**', redirectTo: '/login', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ErrorLogComponent,
    BlackListComponent,
    MainAppViewComponent,
    ManagementComponent,
    UserListComponent,
    MultiGameComponent,
    GamesViewComponent,
    LoginRegistrationComponent,
    GamesHistoryComponent,
    UserDetailsComponent,
    GraduationroadComponent,
    CardGameComponent,
    GeneralErrorComponent,
    AboutComponent,
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,

    FormsModule
  ],
  providers: [ LoginService, UserService, AuthGuard,
              {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
