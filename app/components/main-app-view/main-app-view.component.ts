import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/user.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-app-view',
  templateUrl: './main-app-view.component.html',
  styleUrls: ['./main-app-view.component.css']
})
export class MainAppViewComponent implements OnInit {

  isAdminUser: boolean = false;

  constructor(private userService: UserService, private router: Router) { }


  ngOnInit(): void {
    //this.isAdminUser = this.isUserHasAdmin();
    this.isLoggedIn();
  }

  public routeUserTo(route: string){
    console.log('Routed successfully');
    if(route === 'game'){
      this.router.navigateByUrl(`/games`);
    }else if (route === 'gameHistory'){
      this.router.navigateByUrl(`/games-history`);
    }else if (route === 'uProfile'){
      this.router.navigateByUrl(`/user-details`);
    }
    else if (route === 'admin'){
      this.router.navigateByUrl(`/management`);
    }
  }

  async isLoggedIn(){
    let user = await this.userService.isLoggedIn();
    console.log('appmain view pushed', user);
    this.userService.updateMyUser(user);
  }

  isUserHasAdmin() {
    //const hasAdminRole = this.userService.isUserAdmin();
    //this.isAdminUser = hasAdminRole;
    return this.userService.isUserAdmin();
  }
}
