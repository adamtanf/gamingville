import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import {LoginService} from '../../services/login.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UserAuthDto} from '../../common/user-auth-dto';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class LoginComponent implements OnInit {

  checkoutFormGroup: FormGroup;
  credentials = {
    username: '',
    password: ''
  };

  hasLoginError: boolean = false;

  constructor(private loginService: LoginService, private formBuilder: FormBuilder, private router: Router) {

  }

  ngOnInit(): void {

    if (this.isUserLoggedIn()) {
      this.router.navigateByUrl(`/main`);
    }

    this.showUserForm();
  }

  onSubmit() {
    console.log('form is submitted');

    if (this.checkoutFormGroup.invalid) {
      this.checkoutFormGroup.markAllAsTouched();

      return;
    }

    this.credentials = this.checkoutFormGroup.controls['credentials'].value;

    console.log(this.credentials);
    if ((this.credentials.username != '' && this.credentials.password != '') && (this.credentials.username != null && this.credentials.password != null)) {

      this.loginService.generateToken(this.credentials).subscribe(
        (response: UserAuthDto) => {
          console.log('response: ' + response);
          console.log('token' + response.token);
          this.loginService.loginUser(response.token, response.user);
          window.location.href = '/main';
        },
        error => {
          this.hasLoginError = true;
          console.log(error);
        }
      );
    } else {
      console.log('Fields are empty');
    }
  }

  showUserForm() {
    this.checkoutFormGroup = this.formBuilder.group({
      credentials: this.formBuilder.group({
        username: new FormControl('',
          [Validators.required, Validators.minLength(2)]),
        password: new FormControl('',
          [Validators.required, Validators.minLength(2)])
      })
    });
  }

  get username() { return this.checkoutFormGroup.get('credentials.username'); }
  get password() { return this.checkoutFormGroup.get('credentials.password'); }

  isUserLoggedIn() {
    return this.loginService.isLoggedIn();
  }

}
