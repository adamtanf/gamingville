import { Component, OnInit } from '@angular/core';
import {BlackList} from '../../common/black-list';
import {ActivatedRoute, Router} from '@angular/router';
import {BlackListService} from '../../services/black-list.service';
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-black-list',
  templateUrl: './black-list.component.html',
  styleUrls: ['./black-list.component.css']
})
export class BlackListComponent implements OnInit {

  blackLists: BlackList[] = [];

  thePageNumber: number = 1;
  thePageSize: number = 5;
  theTotalElements: number = 0;

  constructor(private route: ActivatedRoute,
              private blackListService: BlackListService, private router: Router,
               private userService: UserService) { }

  ngOnInit(): void {
    if (this.userService.isUserAdmin() == false) {
      this.router.navigateByUrl(`/main`);
    }
    this.route.paramMap.subscribe( () => {
      this.listBlackList();
    });
  }

  listBlackList() {
    this.handleListBlackList();
  }

  handleListBlackList() {

    this.blackListService.getBlackListList(this.thePageNumber - 1,
      this.thePageSize)
      .subscribe(this.processResult());
  }

  processResult() {
    return data => {
      this.blackLists = data._embedded.blackLists;
      this.thePageNumber = data.page.number + 1;
      this.thePageSize = data.page.size;
      this.theTotalElements = data.page.totalElements;
    };
  }

  updatePageSize(pageSize: number) {
    this.thePageSize = pageSize;
    this.thePageNumber = 1;
    this.listBlackList();
  }

}
