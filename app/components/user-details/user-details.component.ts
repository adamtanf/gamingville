import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/user.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {User} from "../../common/user";
import {environment} from "../../../environments/environment";
import { NoWhiteSpaceValidator } from 'src/app/validators/no-white-space-validator';


@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  serverUrl = `${environment.gameAppUrl}`;
  checkoutFormGroup: FormGroup;
  user: User = new User();

  constructor(private route: ActivatedRoute, private userService: UserService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {

    this.route.queryParams.subscribe((params: Params) => {
      console.log('params:');
      console.log(params);
      if (params['identification'] == null){
        this.showUserForm(this.userService.getUser());
      }else{
        this.showUserForm(
          {
            'name': params['identification'],
            'identification': params['identification']
          }
        );
      }
    });

  }

  showUserForm(userDto) {

    console.log('user : ', userDto);
    this.userService.getUserDetails(userDto).subscribe(
      (response: User) => {
        console.log('user subscribed');
        this.user = response;
        this.checkoutFormGroup = this.formBuilder.group({
          user: this.formBuilder.group({
            userName: new FormControl(this.user.userName,
              [Validators.required,
                Validators.minLength(2),
                NoWhiteSpaceValidator.notOnlyWhitespace]),
            identification: new FormControl(this.user.identification,
              [Validators.required,
                Validators.minLength(5),
                 Validators.pattern('^[0-9]*$'),
                 NoWhiteSpaceValidator.notOnlyWhitespace]),
            email: new FormControl(this.user.email,
              [Validators.required,
                Validators.minLength(6),
                NoWhiteSpaceValidator.notOnlyWhitespace,
               /*
               adam: so here i'm using a regualr expression to make sure a legal email format is inserted:
               first I'm matching a combination of letters and digits with an optional period sign. an '@' sign
               followed by a letters and numbers combo followed by a period, and at the end allow for 2 to 6 chars a to z for
               the email extension

               */
               Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,6}$')]),
            phoneNumber: new FormControl(this.user.phoneNumber,
              [Validators.required,
                Validators.minLength(8),
                Validators.pattern('^[0-9]*$'),
                NoWhiteSpaceValidator.notOnlyWhitespace]),
            role: new FormControl(),
            previousUserName: new FormControl(),
            previousIdentification: new FormControl()
          })
        });
      }, error => {
        console.log(`Error: ${error}`);
      });
  }

  get userName() { return this.checkoutFormGroup.get('user.userName'); }
  get identification() { return this.checkoutFormGroup.get('user.identification'); }
  get email() { return this.checkoutFormGroup.get('user.email'); }
  get phoneNumber() { return this.checkoutFormGroup.get('user.phoneNumber'); }
  get getRole() { return this.checkoutFormGroup.get('user.role'); }
  get getPreviousUserName() { return this.checkoutFormGroup.get('user.previousUserName'); }
  get getPreviousIdentification() { return this.checkoutFormGroup.get('user.previousIdentification'); }


  onSubmit() {
    this.userService.isUserAdmin();
    if (this.checkoutFormGroup.invalid) {
      this.checkoutFormGroup.markAllAsTouched();

      return;
    }

    const tempUser = this.userService.getUser();
    console.log("tempUser");
    console.log(tempUser);
    this.user = this.checkoutFormGroup.controls['user'].value;
    this.user.previousUserName = tempUser.name;
    this.user.previousIdentification = tempUser.identification;
    console.log(this.user);
    console.log(JSON.stringify(this.user));

    this.userService.updateUser(this.user).subscribe(
      response => {
        this.userService.logout();
        this.router.navigateByUrl(`/login`);
      },
      error => {
        console.log(`registration error: ${error}`);
        this.router.navigateByUrl(`/error`);
      }
    );
  }

  hasAdminRole() {
    return this.userService.isUserAdmin();
  }
}
