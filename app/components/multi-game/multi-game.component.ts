import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";
declare var SockJS;
declare var Stomp;
@Component({
  selector: 'app-multi-game',
  templateUrl: './multi-game.component.html',
  styleUrls: ['./multi-game.component.css']
})
export class MultiGameComponent implements OnInit {

  url = 'http://localhost:8080';
  stompClient;
  gameId;
  playerType;
  turns = [['#', '#', '#'], ['#', '#', '#'], ['#', '#', '#']];
  turn = '';
  gameOn = false;
  login = '';
  startGame = false;
  startInterval;
  showPlayer1Turn = false;

  constructor(private httpClient: HttpClient,
              private loginService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.isLoggedIn();
  }
  async isLoggedIn(){
    let user = await this.loginService.isLoggedIn();
    this.loginService.updateMyUser(user);
  }

  connectToSocket(gameId) {
    console.log('connecting to the game');
    let socket = new SockJS(this.url + '/gameplay');
    this.stompClient = Stomp.over(socket);
    this.stompClient.connect({}, (frame) => {
      console.log('connected to the frame: ' + frame);
      this.stompClient.subscribe('/topic/game-progress/' + gameId, (response) => {
        let data = JSON.parse(response.body);
        console.log(data);
        if (this.startGame === false) {
          this.startGame = true;
          // alert('start play');
        }
        this.renderBoard(data);
      });
    });
  }

  connectToGame() {
    this.login = this.loginService.getUser().name;
    if (this.login == null || this.login === '') {
      alert('Please enter login');
    } else {
      const body = {
        name : this.loginService.getUser().name,
        identification : this.loginService.getUser().identification
      };
      console.log("conecting exiting game");
      this.httpClient.post(`${this.url}/game/connect/exist-game`, body).subscribe(
        (response: any) => {
          console.log('response: ' + JSON.stringify(response));
          this.gameId = response.gameId;
          this.playerType = 'O';
          this.reset();
          this.startGame = true;
          this.connectToSocket(this.gameId);
          alert('Start Play');
        },
        error => {
          console.log(error);
        });
    }
  }

  createGame() {
    this.login = this.loginService.getUser().name;
    console.log('this login ' + this.login);
    if (this.login == null || this.login === '') {
      alert('Please enter login');
    } else {


      const body = {
        name : this.loginService.getUser().name,
        identification : this.loginService.getUser().identification
      };

      this.httpClient.post(`${this.url}/game/create-game`, body).subscribe(
      (response: any) => {
        console.log('response: ' + response);
        this.gameId = response.gameId;
        this.playerType = 'X';
        this.reset();
        this.connectToSocket(this.gameId);
        alert('Your created a game. Game id is: ' + response.gameId);
        this.gameOn = true;
        this.startPolling();
      },
      error => {
        console.log(error);
      });
    }
  }

  reset() {
    this.turns = [["#", "#", "#"], ["#", "#", "#"], ["#", "#", "#"]];
  }

  clickBoard(id, i, j) {
    this.playerTurn(this.turn, id, i, j);
  }

  playerTurn(turn, id, i, j) {
    if (this.gameOn) {
      const spotTaken = this.turns[i][j];
      if (spotTaken === "#") {
        this.step(this.playerType, id.split("_")[0], id.split("_")[1]);
      }
    }
  }

  step(type, x, y) {

    const data = {
      "type": type,
      "coordinateX": x,
      "coordinateY": y,
      "gameId": this.gameId
    };

    this.httpClient.post(`${this.url}/game/gameplay`, data).subscribe(
      (response: any) => {
        console.log('response: ' + response);
        this.gameOn = false;
        this.renderBoard(response);
      },
      error => {
        console.log(error);
      });
  }

  renderBoard(data) {
    console.log("in displayResponse");
    console.log(`data = ${JSON.stringify(data)}`);
    let board = data.board;
    if (board == null || board === ''){
      alert('Error board is empty');
    }
    for (let i = 0; i < board.length; i++) {
      for (let j = 0; j < board[i].length; j++) {
        if (board[i][j] === 1) {
          this.turns[i][j] = 'X';
        } else if (board[i][j] === 2) {
          this.turns[i][j] = 'O';
        }
      }
    }
    if (data.winner != null) {
      alert('Winner is ' + data.winner);
      this.router.navigateByUrl(`/main`);
    }
    this.gameOn = true;
  }

  startPolling() {
    this.startInterval = setInterval(() => {
       this.getPlayer1Turn();
    }, 5000);
  }

  getPlayer1Turn() {
    const data = this.gameId;
    this.httpClient.post(`${this.url}/game/player-one-turn`, data).subscribe(
      (response: boolean) => {
        console.log('response: ' + response);
        if (response == true) {
          this.showPlayer1Turn = true;
          clearInterval(this.startInterval);
        }
      },
      error => {
        console.log(error);
      });
  }
}
