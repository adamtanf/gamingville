import { Component, OnInit } from '@angular/core';
import {UserGameHistory} from "../../common/user-game-history";
import {GameService} from "../../services/game.service";
import {error} from "protractor";

@Component({
  selector: 'app-games-history',
  templateUrl: './games-history.component.html',
  styleUrls: ['./games-history.component.css']
})
export class GamesHistoryComponent implements OnInit {

  gameList: UserGameHistory[] = [];

  thePageNumber: number = 1;
  thePageSize: number = 5;
  theTotalElements: number = 0;
  pageSlice: UserGameHistory[] = [];

  constructor(private gameService: GameService) { }

  ngOnInit(): void {
    this.showUserGamesHistory();
  }

  showUserGamesHistory() {
    this.gameService.getUserGamesHistory().subscribe(
      (response: UserGameHistory[]) => {
        this.gameList = response;
        this.theTotalElements = this.gameList.length;
        this.pageSlice = this.gameList.slice(0, 5);
      }, error => {
        console.log('error in getting all games history');
      }
    );
  }

  updatePageSize(pageSize: number) {
    console.log(`page size: ${pageSize}`);
    this.thePageSize = pageSize;
    this.thePageNumber = 1;
    this.listGamesHistory();
  }

  listGamesHistory() {
    console.log(`page size: ${this.thePageSize}`);
    console.log(`thePageNumber: ${this.thePageNumber}`);
    const startIndex = (this.thePageNumber - 1) * this.thePageSize;
    let endIndex = startIndex + this.thePageSize;
    if (endIndex > this.gameList.length) {
      endIndex = this.gameList.length;
    }
    console.log(`startIndex: ${startIndex}`);
    console.log(`endIndex: ${endIndex}`);
    console.log(`this.gameList.length: ${this.gameList.length}`);
    this.pageSlice = this.gameList.slice(startIndex, endIndex);
  }
}
