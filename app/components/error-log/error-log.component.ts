import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ErrorLogService} from '../../services/error-log.service';
import {ErrorLog} from '../../common/error-log';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-error-log',
  templateUrl: './error-log.component.html',
  styleUrls: ['./error-log.component.css']
})
export class ErrorLogComponent implements OnInit {

  errorLogs: ErrorLog[] = [];

  thePageNumber: number = 1;
  thePageSize: number = 5;
  theTotalElements: number = 0;

  constructor(private route: ActivatedRoute,
              private errorLogService: ErrorLogService,
              private router: Router,
              private userservice: UserService) { }

  ngOnInit(): void {

    if (this.userservice.isUserAdmin() == false){
      this.router.navigateByUrl('/main');
    }
    this.route.paramMap.subscribe( () => {
      this.listErrorLogs();
    });
  }

  listErrorLogs() {
    this.handleListErrorLogs();
  }

  handleListErrorLogs() {


    this.errorLogService.getErrorLogList(this.thePageNumber - 1,
      this.thePageSize)
      .subscribe(this.processResult());
  }

  processResult() {
    return data => {
      this.errorLogs = data._embedded.errorLogs;
      this.thePageNumber = data.page.number + 1;
      this.thePageSize = data.page.size;
      this.theTotalElements = data.page.totalElements;
    };
  }

  updatePageSize(pageSize: number) {
    this.thePageSize = pageSize;
    this.thePageNumber = 1;
    this.listErrorLogs();
  }
}
