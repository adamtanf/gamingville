import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/user.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {User} from "../../common/user";
import {Router} from "@angular/router";
import { NoWhiteSpaceValidator } from 'src/app/validators/no-white-space-validator';

@Component({
  selector: 'app-login-registration',
  templateUrl: './login-registration.component.html',
  styleUrls: ['./login-registration.component.css']
})
export class LoginRegistrationComponent implements OnInit {

  checkoutFormGroup: FormGroup;
  user: User = new User();

  constructor(private userService: UserService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.showUserForm();
  }

  showUserForm() {
    this.checkoutFormGroup = this.formBuilder.group({
      user: this.formBuilder.group({
        userName: new FormControl('',
          [Validators.required,
            Validators.minLength(2),
            NoWhiteSpaceValidator.notOnlyWhitespace]),
        password: new FormControl('',
          [Validators.required,
             Validators.minLength(4),
             NoWhiteSpaceValidator.notOnlyWhitespace]),
        identification: new FormControl('',
          [Validators.required,
             Validators.minLength(5),
              Validators.pattern('^[0-9]*$'),
              NoWhiteSpaceValidator.notOnlyWhitespace]),
        email: new FormControl('',
          [Validators.required,
             Validators.minLength(6),
             NoWhiteSpaceValidator.notOnlyWhitespace,
            /*
            adam: so here i'm using a regualr expression to make sure a legal email format is inserted:
            first I'm matching a combination of letters and digits with an optional period sign. an '@' sign
            followed by a letters and numbers combo followed by a period, and at the end allow for 2 to 6 chars a to z for
            the email extension

            */
            Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,6}$')
          ]),
        phoneNumber: new FormControl('',
          [Validators.required,
             Validators.minLength(8),
             Validators.pattern('^[0-9]*$'),
             NoWhiteSpaceValidator.notOnlyWhitespace
            ])
      })
    });
  }

  onSubmit() {
    console.log('form is submitted');

    if (this.checkoutFormGroup.invalid) {
      this.checkoutFormGroup.markAllAsTouched();

      return;
    }

    this.user = this.checkoutFormGroup.controls['user'].value;

    this.userService.isUserNameValid(this.user.userName).subscribe(
      (response: boolean) => {
        if (response != true) {
          console.log(`registration error: ${response}`);
          this.router.navigateByUrl(`/error`);
        }
      },
      error => {
        console.log(`registration error: ${error}`);
        this.router.navigateByUrl(`/error`);
      }
    );

    console.log(JSON.stringify(this.user));
    this.userService.registerUser(this.user).subscribe(
      response => {
        this.router.navigateByUrl(`/main`);
      },
      error => {
        console.log(`registration error: ${error}`);
        this.router.navigateByUrl(`/error`);
      }
    );
  }

  get userName() { return this.checkoutFormGroup.get('user.userName'); }
  get password() { return this.checkoutFormGroup.get('user.password'); }
  get identification() { return this.checkoutFormGroup.get('user.identification'); }
  get email() { return this.checkoutFormGroup.get('user.email'); }
  get phoneNumber() { return this.checkoutFormGroup.get('user.phoneNumber'); }
}
