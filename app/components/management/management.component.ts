import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.css']
})
export class ManagementComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    // if (this.userService.isUserAdmin()==false){
    //   this.router.navigateByUrl('/main');
    // }
    this.isLoggedIn();
  }
  async isLoggedIn(){
    let user = await this.userService.isLoggedIn();
    this.userService.updateMyUser(user);
  }

}
