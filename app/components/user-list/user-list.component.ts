import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UserListDto} from "../../common/user-list-dto";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  userList: UserListDto[] = [];

  thePageNumber: number = 1;
  thePageSize: number = 5;
  theTotalElements: number = 0;

  constructor(private route: ActivatedRoute,
     private userService: UserService,
     private userservice: UserService,
     private router: Router) { }

  ngOnInit(): void {
    if (this.userservice.isUserAdmin() == false){
      this.router.navigateByUrl('/main');
    }
    this.route.paramMap.subscribe( () => {
      this.listUsers();
    });
  }

  listUsers() {
    this.userService.getListOfUsers(this.thePageNumber - 1, this.thePageSize).subscribe(this.processResult());
    console.log(this.userList);
  }

  processResult() {
    return data => {
      this.userList = data._embedded.users;
      console.log(this.userList);
      this.thePageNumber = data.page.number + 1;
      this.thePageSize = data.page.size;
      this.theTotalElements = data.page.totalElements;
    };
  }

  updatePageSize(pageSize: number) {
    this.thePageSize = pageSize;
    this.thePageNumber = 1;
    this.listUsers();
  }

  unBlockUser(name, identification) {
    const json = {
      'name': name,
      'identification': identification
    };

    this.userService.unBlockUser(json).subscribe(
      response => {
        this.ngOnInit();
      },
      error => {
        this.router.navigateByUrl(`/error`);
      }
    );
  }

  blockUser(name, identification) {
    const json = {
      'name': name,
      'identification': identification
    };

    this.userService.blockUser(json).subscribe(
      response => {
        this.ngOnInit();
      },
      error => {
        this.router.navigateByUrl(`/error`);
      }
    );
  }

  editUser(userName, id) {
    this.router.navigate( ['user-details', {identification: id, name:
      userName}]);
  }
}
