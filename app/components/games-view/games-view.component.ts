import { Component, OnInit } from '@angular/core';
import {GameService} from "../../services/game.service";
import {Game} from "../../common/game";
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-games-view',
  templateUrl: './games-view.component.html',
  styleUrls: ['./games-view.component.css']
})
export class GamesViewComponent implements OnInit {

  gameList: Game[] = [];
  constructor(private gameService: GameService, private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.showListOfGames();
    this.isLoggedIn();
  }
  async isLoggedIn(){
    let user = await this.userService.isLoggedIn();
    this.userService.updateMyUser(user);
  }

  showListOfGames() {
    this.gameService.getAllGames().subscribe(
      response => {
        console.log('showListOfGames ' + response._embedded.games);
        this.gameList = response._embedded.games;
      },
      error => {
        console.log(`error: ${error}`);
      });
  }

  public loadGame(game: string){
    console.log('Routed successfully')
    if(game === 'tic'){
      this.router.navigateByUrl(`/tic-tac-toe`);
    }else if (game === 'card'){
      this.router.navigateByUrl(`/card-game`);
    }else{
      this.router.navigateByUrl(`/graduation-road`);
    }
  }
}
