import {Component, OnDestroy, OnInit} from '@angular/core';
import * as Phaser from 'phaser';
import {GameService} from "../../../services/game.service";
import {environment} from "../../../../environments/environment";
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';


/* Preloader class: */
export default class Preloader extends Phaser.Scene {
  constructor() {
    super({key: 'Preloader'})

  }

  preload() {
    console.log('preload method');
    this.load.image('sky', 'assets/sky.png');
    this.load.image('end', 'assets/end.png');
    this.load.image('ground', 'assets/platform.png');
    this.load.image('star', 'assets/star.png');
    this.load.image('bomb', 'assets/bomb.png');
    this.load.spritesheet('dude', 'assets/dude.png', {frameWidth: 32, frameHeight: 48});
    this.load.image('menu', 'assets/menu.png');
    this.load.image('school', 'assets/school.png');
    this.load.image('play', 'assets/play.png');
    this.load.image('instructions', 'assets/instructions.png');
    this.load.image('begin', 'assets/begin.png');
    this.load.image('success', 'assets/success.png');


    // audio

    this.load.audio('gamesound', 'assets/theme.mp3')
    this.load.audio('collect', 'assets/collect.wav')
    this.load.audio('menuSong', 'assets/menuSong.m4a')

    this.load.audio('death', 'assets/death.wav')


  }

  create() {
    console.log("game is booting...");


    this.scene.start('menu')

  }


}/* end of Preloader classd */

/* Main scene class: This class represents my main scene where the game takes place */
class MainScene extends Phaser.Scene {

  constructor(private gameService: GameService) {
    super({key: 'main'});

  }

  player: any;
  stars: any;
  bombs: any;
  private platforms?: Phaser.Physics.Arcade.StaticGroup; //I put a "?" to mark that platforms is optional
  cursors: any;
  private score = 0;
  gameOver = false;
  private scoreText!: Phaser.GameObjects.Text;

  init() {
    this.score = 0;

  }


  create() {
    //this.sound.add('gamesound', {loop: true}).play();

    this.sound.add('menuSong', {loop: true}).play();

    console.log('create method');
    this.add.image(400, 300, 'school');
    this.platforms = this.physics.add.staticGroup()
    const group = this.platforms.create(400, 568, 'ground').setScale(2).refreshBody();

    this.platforms.create(600, 400, 'ground');
    this.platforms.create(50, 250, 'ground');
    this.platforms.create(750, 220, 'ground');

    this.player = this.physics.add.sprite(100, 450, 'dude');
    this.player.setBounce(0.2);
    this.player.setCollideWorldBounds(true);

    this.anims.create({
      key: 'left',
      frames: this.anims.generateFrameNumbers('dude', {start: 0, end: 3}),
      frameRate: 10,
      repeat: -1
    });

    this.anims.create({
      key: 'turn',
      frames: [{key: 'dude', frame: 4}],
      frameRate: 20
    });

    this.anims.create({
      key: 'right',
      frames: this.anims.generateFrameNumbers('dude', {start: 5, end: 8}),
      frameRate: 10,
      repeat: -1
    });

    this.cursors = this.input.keyboard.createCursorKeys();

    this.stars = this.physics.add.group({
      key: 'star',
      repeat: 11,
      setXY: {x: 12, y: 0, stepX: 70}
    });


    this.stars.children.iterate(function (child: any) {

      //  Give each star a slightly different bounce
      child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8));

    });

    this.bombs = this.physics.add.group();
    this.scoreText = this.add.text(16, 16, 'score: 0', {fontSize: '32px'});//left out fill property check what happens

    this.physics.add.collider(this.player, this.platforms);
    this.physics.add.collider(this.stars, this.platforms);
    this.physics.add.collider(this.bombs, this.platforms);

    this.physics.add.overlap(this.player, this.stars, this.collectStar, undefined, this); // undefined instead of null

    this.physics.add.collider(this.player, this.bombs, this.hitBomb, undefined, this); // undifined instead of nulll


  }


////


  collectStar(player: any, star: any) {
    star.disableBody(true, true);
    const hitSound = this.sound.add('collect', {loop: false});
    hitSound.play();

    //  Add and update the score
    this.score += 1;
    if (this.score == 120) {
      this.gameService.registerGame(environment.graduationRoadGameId, 120);
      this.scene.stop();
      this.scene.start('success');
    }
    this.scoreText.setText('Score: ' + this.score);

    if (this.stars.countActive(true) === 0) {
      //  A new batch of stars to collect
      this.stars.children.iterate(function (child: any) {

        child.enableBody(true, child.x, 0, true, true);

      });

      var x = (player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);

      var bomb = this.bombs.create(x, 16, 'bomb');
      bomb.setBounce(1);
      bomb.setCollideWorldBounds(true);
      bomb.setVelocity(Phaser.Math.Between(-200, 200), 20);
      bomb.allowGravity = false;

    }
  }


/////

  hitBomb(player: any, bomb: any) {
    const death = this.sound.add('death', {loop: false});
    death.play();
    this.physics.pause();

    player.setTint(0xff0000);

    player.anims.play('turn');

    this.gameOver = true;
    this.scene.start('gameover', {score: this.score});

    // this.scene.start('GameOverScene',{score: this.score})
  }


  update() {
    if (this.cursors.left.isDown) {
      this.player.setVelocityX(-160);

      this.player.anims.play('left', true);
    } else if (this.cursors.right.isDown) {
      this.player.setVelocityX(160);

      this.player.anims.play('right', true);
    } else {
      this.player.setVelocityX(0);

      this.player.anims.play('turn');
    }

    if (this.cursors.up.isDown && this.player.body.touching.down) {
      this.player.setVelocityY(-330);
    }


  }

} // end of main scene class


/* Menu scene */
class MenuScene extends Phaser.Scene {


  //music.pauseOnBlur = true; // song keeps playing even if game frame is out of focus

  constructor() {
    super({key: 'menu'})

  }

  create() {
    this.add.image(0, 0, 'menu').setOrigin(0, 0);
    let playButton = this.add.image(this.game.renderer.width / 2, this.game.renderer.height / 2, 'play')
    playButton.setInteractive();
    // const music = this.sound.add('menuSong', {loop: false});
    // music.play();

    playButton.on('pointerup', () => {

      this.scene.start('ins')
    })


  }

}/*End of menu scene */

/* Instructions scene */
class Instructions extends Phaser.Scene {

  constructor() {
    super({key: 'ins'})

  }

  init(data: any) {
    data;
  }

  create() {

    this.add.image(0, 0, 'instructions').setOrigin(0, 0);
    let playButton = this.add.image(this.game.renderer.width / 2, this.game.renderer.height / 2.1, 'begin')
    playButton.setInteractive();

    playButton.on('pointerup', () => {

      this.scene.start('main')
    })


  }

}/*End of Instructions scene */

/*Game over scene*/
class GameOverScene extends Phaser.Scene {


  retryButton = '';
  score?: integer;

  init(data: any) {
    this.score = data.score;
  }

  constructor() {
    super({key: 'gameover'});
    console.log('dead succeded')

  }


  create() {
    this.add.image(0, 0, 'end').setOrigin(0, 0);
    const {width, height} = this.scale
    const x = width * 0.5
    const y = height * 0.5
    this.add.text(x, y, 'GAME OVER\n' + 'FINAL SCORE: ' + this.score + '\nPress SPACE to Play Again', {
      fontSize: '32px',
      color: '#FFFFFF',
      backgroundColor: '#000000',
      shadow: {fill: true, blur: 0, offsetY: 0},
      padding: {left: 15, right: 15, top: 10, bottom: 10}
    }).setOrigin(0.5)


    this.input.keyboard.once('keydown-SPACE', () => {
      //stop the current scene
      this.scene.stop();
      this.scene.stop('main');
      this.scene.start('main')
    })

    /* this.add.text(640,360, "GAME OVER/n" + "Score: " + this.score,{
       fontSize: '60px',
       color: "#000000",
       fontStyle: "bold",
       backgroundColor: "#FFFFFF",
       padding: {left: 15, right: 15, top: 10, bottom: 10}
     }).setOrigin(0.5);*/


    //this.retryButton = this.add.text()

  }
}/*End of gamer over scene*/


/*success scene*/
class Success extends Phaser.Scene {


  constructor() {
    super({key: 'success'});


  }


  create() {
    this.add.image(0, 0, 'success').setOrigin(0, 0);


  }
}/*End of success over scene*/


@Component({
  selector: 'graduation-road',
  templateUrl: './graduation-road.component.html',
  styleUrls: ['./graduation-road.component.css']
})
export class GraduationroadComponent implements OnInit, OnDestroy {
  phaserGame: Phaser.Game | undefined;
  config: Phaser.Types.Core.GameConfig;

  constructor(private userService: UserService, private router: Router) {
    this.config = {
      type: Phaser.AUTO,
      height: 600,
      width: 800,
      scene: [Preloader, MenuScene, Instructions, MainScene, GameOverScene, Success],
      parent: 'gameContainer',
      physics: {
        default: 'arcade',
        arcade: {
          gravity: {y: 300},
          debug: false // helps to debug by puting lines around an image or sprite's border
        }
      }
    };
  }
  ngOnDestroy(): void {
    console.log('killing game');
    this.phaserGame.destroy(true, false);
  }

  ngOnInit() {
    this.phaserGame = new Phaser.Game(this.config);
    this.isLoggedIn();
  }
  async isLoggedIn(){
    let user = await this.userService.isLoggedIn();
    this.userService.updateMyUser(user);
  }
}
