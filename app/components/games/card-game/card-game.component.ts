import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'card-game',
  templateUrl: './card-game.component.html',
  styleUrls: ['./card-game.component.css']
})
export class CardGameComponent implements OnInit {
  constructor(private elementRef:ElementRef, private _cd: ChangeDetectorRef,private userService: UserService, private router: Router){


  }
  ngOnInit(): void {
    this.image1 = false;
    this.selectedOption = 'choose';
    this.isLoggedIn();
  }
  async isLoggedIn(){
    let user = await this.userService.isLoggedIn();
    this.userService.updateMyUser(user);
  }

  allColors = [
    "assets/bird.png",
    "assets/butterfly.png",
    "assets/cat.png",
    "assets/giraffe.png",
    "assets/lion.png",
    "assets/dog.png",
  ];
  title = 'game';
  colors: any = [];
  score: number = 0;
  pickedColors: Array<string> = [];
  @ViewChild('gameBoard') gameBoard: any;
  @ViewChild('imageModal') imageModal: any;
@ViewChild('uploadImageSection') uploadImageSection: any
@ViewChild('modalP') modalP: any;
@ViewChild('modalWarning') modalWarning: any;
@ViewChild('scoreValue') scoreValue: any;
@ViewChild('image1') image1: any;
@ViewChild('image2') image2: any;
@ViewChild('image3') image3: any;
@ViewChild('image4') image4: any;
@ViewChild('image5') image5: any;
  option: Array<string> = ['easy', 'medium', 'hard'];
  img1: boolean = false;
  img2: boolean = false;
  img3: boolean = false;
  img4: boolean = false;
  img5: boolean = false;
  myHtmlProperty: string = 'All Cards Are Matched!';

  selectedOption: any;
  inpFile: any;

    // To Do: check for uploads



    public custombuttonClick(){
      // this.imageModal.style.display = "block";
      this.imageModal.nativeElement.setAttribute('style', 'display: block');
      this.inpFile = [this.uploadImageSection.nativeElement.getElementsByTagName("input")];
      this.uploadImage();
    }

    public uploadImage() {

      let allColors = []; // empty the default array that has animals as default.
      //  Grab Uploaded files:


      this.inpFile.forEach((item: any, index: string) => {

        // let previewImage = document.createElement("img");
        // previewImage.classList.add("previewImag");
        // previewImage.setAttribute("id", "image" + index);
        // console.log("previewImage", previewImage)


      });
    }

    public change(files: any, image: any): any{
      const file = files.target.files[0];
      // let parentNodetoAppend = this.parentElement;

      if (file) {
        const reader = new FileReader();
        switch(image){
          case 'image1':
          this.image1.nativeElement.src = file.src;
          this.image1.nativeElement.setAttribute('style', 'display: block');
          this.allColors.push(file.src);
          break;
          case 'image2':
          this.image2.nativeElement.src = file.src;
          this.image2.nativeElement.setAttribute('style', 'display: block');
          this.allColors.push(file.src);
          break;
          case 'image3':
          this.image3.nativeElement.src = file.src;
          this.image3.nativeElement.setAttribute('style', 'display: block');
          this.allColors.push(file.src);
          break;
          case 'image4':
          this.image4.nativeElement.src = file.src;
          this.image4.nativeElement.setAttribute('style', 'display: block');
          this.allColors.push(file.src);
          break;
          case 'image5':
          this.image5.nativeElement.src = file.src;
          this.image5.nativeElement.setAttribute('style', 'display: block');
          this.allColors.push(file.src);
          break;
          default:
            console.log('default')
        }
        // reader.onload = function() {
        //   previewImage.src = this.result as any;
        //   parentNodetoAppend.appendChild(previewImage);  // show the image preview.
        //   previewImage.style.display = "block";
        //   allColors.push(this.result); //replace the new images with the default ones (animals).

        // }
        // reader.readAsDataURL(file);
      }
    }




    public onChange(event: any){
      // selectedOption = this.value;
      this.colors = []; // clear the array content to prevent overriding when user changes the option everytime.
      switch (this.selectedOption) {
        case "easy": // choose 4 random colors
          this.chooseRandomColors(4);
          break;
        case "medium":
          this.chooseRandomColors(5);
          break;
        case "hard":
          this.chooseRandomColors(6);
          break;
        default:
          this.chooseRandomColors(4);
      }

    }
    public chooseRandomColors(numberOfCards: number) {
      // choose 4 random index numbers
    let tempAllColors: any = []; // Create a temporary array.
    this.allColors.forEach(item => {
      tempAllColors.push(item)
    }); // duplice the allColors array as tempAllColors.

    this.shuffle(tempAllColors); // shuffle the array to choose random colors.

    for (let i = 0; i < numberOfCards; i++) {
      this.colors.push(tempAllColors[i]);
    }
    }


   firstCard:any;
   secondCard:any;

   firstCardColor:any;
   secondCardColor:any;

   scoreCounter = 0;

     // Shuffle the cards so that the order of the color will be different.
     public shuffle(array:any) {
      for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * i);
        const temp = array[i];
        array[i] = array[j];
        array[j] = temp;
      }
    }




    public createBoard(): false| void{
    // First check whether an option is selected. Warn the user if not.
    this.score = 0;
    // this.updateScore(this.scoreCounter);

    if (this.selectedOption === "choose") {
      this.myHtmlProperty = "Choose an Option!";
      this.modalWarning.nativeElement.setAttribute('style', 'display: block');
      return false;
    }

     this.pickedColors = [] as any;
    for (let i = 0; i < this.colors.length; i++) {
      this.pickedColors.push(this.colors[i]);
      this.pickedColors.push(this.colors[i]); // duplicate the elements inside the array.
      // ["red", "red", "blue", "blue", "green", "green"]

    }


    this.shuffle(this.pickedColors); // shuffle the cards.
    // First, check if a set of cards already exist. IF they do, delete them all and replace with new ones.

    if (this.gameBoard.nativeElement.children.length === 0) {
      // If no cards exist in the board, create them:

      for (let i = 0; i < this.pickedColors.length; i++) {

        let backgroundImage = 'url(' + this.pickedColors[i] + ')'; /// THIS IS THE PART WHERE THE IMAGE IS ATTACHED TO THE FRONT OF THE CARD.

      }

      // document.querySelectorAll(".flip-card").forEach(item => {
      //   item.addEventListener("click", handleClick); // add event listener for all the cards on the board.
      // });
      // this._cd.reattach();
    } else {
      this.gameBoard.innerHTML = "";
      this.createBoard();
    }
  }

  countInArray(array: [], what: string):any {
    var count = 0;
    for (var i = 0; i < array.length; i++) {
        if (array[i] === what) {
            count++;
        }
    }
    return count;
}
openCards:any  = [];
  public handleClick(event?: any) {
    console.log('done');
    if (event.currentTarget.classList.contains("matched")) {  // if (event.currentTarget.contains("matched")) {
      console.log("do nothing.");
      return;

    } else {
      const numberOfOnCards: any = this.CardsOn();
      if (numberOfOnCards === 0) {
        event.currentTarget.firstElementChild.classList.toggle("animate"); // if none or one card is already on, you can flip over another card.
        this.firstCardColor = event.currentTarget.firstElementChild.childNodes[1].style.backgroundImage; // keep the first card's color in a variable.
        this.firstCard = event.currentTarget;

     } else if (numberOfOnCards === 1) {
        // console.log("here i am:", this.firstElementChild);
        event.currentTarget.firstElementChild.classList.toggle("animate"); // if none or one card is already on, you can flip over another card.
        this.secondCardColor = event.currentTarget.firstElementChild.childNodes[1].style.backgroundImage;
        this.secondCard = event.currentTarget;
      setTimeout(()=>{                           //<<<---using ()=> syntax
        this.checkSameCards();
   }, 1000);


      //   }
      } else {
        this.firstCard.firstElementChild.classList.toggle("animate");
      this.secondCard.firstElementChild.classList.toggle("animate");
      console.log("I'm hereeee", this.secondCard.firstElementChild);

      }


    }


  }
  public updateScore(value: number) {
    this.score = this.score + value;
    this._cd.detectChanges();
  }

  public CardsOn(): any {
    let counter = 0;

    const allCards = document.querySelectorAll(".flip-card");
    allCards.forEach(item => {
      if (item.firstElementChild?.classList.contains("animate") && !item.classList.contains("matched")) {
        counter++;
        console.log(counter);
      }

    });
    return counter;

  }

  public checkSameCards(): any {
    if (this.firstCard !== this.secondCard && this.firstCardColor === this.secondCardColor) {

      this.firstCard.classList.add("matched");
      this.secondCard.classList.add("matched");
      // this.checkMatchedCards();

      this.score ++;
      // this.updateScore(this.scoreCounter);
      if(this.colors.length === this.score){
        this.myHtmlProperty = 'Congratulations!!!! You Won.'
        this.modalWarning.nativeElement.setAttribute('style', 'display: block');
      }

    } else {
      this.firstCard.firstElementChild.classList.toggle("animate");
      this.secondCard.firstElementChild.classList.toggle("animate");
      console.log("I'm hereeee", this.secondCard.firstElementChild);
     // Todo: changethis.hasMatchedCards = false;
    }
  }

  public checkMatchedCards() {

    const allCards: any = [];// Todo: change [...document.querySelectorAll(".flip-card")];
    const isAllOn = allCards.every((item:any) => {
      return item.classList.contains("matched");
    });
    if (isAllOn) {
      // To Do: modal.style.display = "block";
    }
  }

   modal = document.querySelector(".modal");
 modalImage = document.getElementById("imageModal");
 spanWarning = document.getElementsByClassName("close")[0];
 spanImage = document.getElementsByClassName("close")[1];

//  spanWarning.onclick = function() {
//   modalWarning.style.display = "none";
// }
// spanImage.onclick = function() {
//   modalImage.style.display = "none";
// }

// When the user clicks anywhere outside of the modal, close it
// window.onclick = function(event) {
//   if (event.target == modalWarning) {
//     modalWarning.style.display = "none";
//   } else if (event.target == modalImage) {
//     modalImage.style.display = "none";

//   }
// }

closeWarn(){
  this.modalWarning.nativeElement.setAttribute('style', 'display: none');
}
closeUpload(){
  this.imageModal.nativeElement.setAttribute('style', 'display: none');
}
}
